package telegram_pkg

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"go.opentelemetry.io/otel"
	"net/http"
)

const (
	TELEGRAM_URL          = "https://api.telegram.org/bot"
	TELEGRAM_SEND_MESSAGE = "/sendMessage"
)

var (
	chatId  string
	botKey  string
	appName string
)

type Telegram struct{}

func New() *Telegram {
	return &Telegram{}
}

type Options struct {
	ChatId  string
	BotKey  string
	AppName string
}

func Register(arg Options) {
	chatId = arg.ChatId
	botKey = arg.BotKey
	appName = arg.AppName
}

type Message struct {
	Body  string
	Trace string
}

func (t *Telegram) SendMessage(ctx context.Context, arg Message) error {
	traceCtx, span := otel.Tracer("").Start(ctx, "SendMessage")
	defer span.End()

	if chatId == "" || botKey == "" {
		return errors.New("invalid chatId or botKey")
	}

	url := TELEGRAM_URL + botKey + TELEGRAM_SEND_MESSAGE
	content := fmt.Sprintf("Приложение: <b>%s</b>\n%s\nТрассировка: <code>%s</code>", appName, arg.Body, arg.Trace)
	payload, _ := json.Marshal(map[string]string{
		"chat_id":    chatId,
		"text":       content,
		"parse_mode": "html",
	})

	request, err := http.NewRequestWithContext(traceCtx, http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		return fmt.Errorf("fail create request - %w", err)
	}

	request.Header.Add("Content-Type", "application/json")

	client := http.DefaultClient
	res, err := client.Do(request)
	if err != nil {
		fmt.Println(err)
	}

	_ = res.Body.Close()
	if err != nil {
		fmt.Println(err)
	}

	return nil
}

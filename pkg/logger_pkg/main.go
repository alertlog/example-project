package logger_pkg

import (
	"fmt"
	"golang.org/x/exp/slog"
	"log"
	"notification/pkg/fs_pkg"
	"os"
	"runtime"
	"strings"
	"time"
)

type Logger struct {
	traceId string
	stdlog  *slog.Logger
}

var stdlog *slog.Logger

func Register() {
	stdlog = slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{}))
}

func New(traceId string) *Logger {
	return &Logger{traceId: traceId, stdlog: stdlog}
}

// логирование уровня info
func (l *Logger) Success(content any) {
	_, filename, line, ok := runtime.Caller(1)
	var infoContent string
	if ok {
		infoContent = fmt.Sprintf("%+v %s:%d [%s]", content, formatFileName(filename), line, l.traceId)
	} else {
		infoContent = fmt.Sprintf("%+v [%s]", content, l.traceId)
	}
	l.stdlog.Info(infoContent, slog.String("trace_id", l.traceId))
	// loggerWriter(infoContent, "success")
}

// логирование уровня info
func (l *Logger) Log(content any) {
	_, filename, line, ok := runtime.Caller(1)
	var infoContent string
	if ok {
		infoContent = fmt.Sprintf("%+v %s:%d [%s]", content, formatFileName(filename), line, l.traceId)
	} else {
		infoContent = fmt.Sprintf("%+v [%s]", content, l.traceId)
	}
	// log.Println(teal("[info] "), infoContent)
	l.stdlog.Info(infoContent, slog.String("trace_id", l.traceId))
	// loggerWriter(infoContent, "info")
}

// логирование уровня info
func (l *Logger) Warn(content any) {
	_, filename, line, ok := runtime.Caller(1)
	var infoContent string
	if ok {
		infoContent = fmt.Sprintf("%+v %s:%d [%s]", content, formatFileName(filename), line, l.traceId)
	} else {
		infoContent = fmt.Sprintf("%+v [%s]", content, l.traceId)
	}
	l.stdlog.Warn(infoContent, slog.String("trace_id", l.traceId))
	// log.Print(yellow("[warn] "))
	// log.Println(yellow("[warn] "), infoContent)
	loggerWriter(infoContent, "warn")
}

// логирование уровня info
func (l *Logger) Error(content any) {
	_, filename, line, ok := runtime.Caller(1)
	var infoContent string
	if ok {
		infoContent = fmt.Sprintf("%+v %s:%d [%s]", content, formatFileName(filename), line, l.traceId)
	} else {
		infoContent = fmt.Sprintf("%+v [%s]", content, l.traceId)
	}
	l.stdlog.Error(infoContent, slog.String("trace_id", l.traceId))
	// log.Println(red("[error] "), infoContent)
	// loggerWriter(infoContent, "error")
}

// инициализация логгера, создание необходимого файла
func loggerWriter(content string, level string) {
	if err := fs_pkg.CheckFolder("./logs"); err != nil {
		if err := fs_pkg.CreateFolder("./logs"); err != nil {
			log.Fatal("error create logs folder")
		}
	}
	fileName := level + "." + "log"
	file, err := os.OpenFile("./logs/"+fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0o666)
	if err != nil {
		log.Fatal("error opening file: ", err)
	}
	defer file.Close()
	_, _ = file.WriteString(content + " " + time.Now().Format(time.RFC3339) + "\n")
}

func formatFileName(arg string) string {
	if len(arg) == 0 {
		return ""
	}

	list := strings.Split(arg, "/")
	for i, j := 0, len(list)-1; i < j; i, j = i+1, j-1 {
		list[i], list[j] = list[j], list[i]
	}

	var result string
	if len(list) >= 2 {
		result = list[1] + "/" + list[0]
	} else {
		result = list[0]
	}
	return result
}

package shutdown_pkg

import (
	"context"
	"fmt"
	"log"
	"notification/pkg/telegram_pkg"
	"os"
	"os/signal"
	"syscall"
)

func (m *Manager) StartGracefulShutdown(ctx context.Context) {
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)

	select {
	case sig := <-sigChan:
		log.Printf("Received signal: %v. Initiating graceful shutdown...\n", sig)
		_ = telegram_pkg.New().SendMessage(ctx, telegram_pkg.Message{Body: fmt.Sprintf("notification shutdown")})

		for _, handler := range m.handlers {
			handler()
		}

		m.wg.Wait()

		log.Println("Graceful shutdown completed.")

	case <-ctx.Done():
		log.Println("Graceful shutdown initiated through context cancellation.")
	}
}

package shutdown_pkg

import (
	"context"
	"os"
	"sync"
)

type ShutdownHandler func()

type Manager struct {
	wg             *sync.WaitGroup
	shutdownSignal chan os.Signal
	handlers       []ShutdownHandler
}

type ShutdownManager interface {
	AddHandler(handler ShutdownHandler)
	RegisterWaitGroup(wg *sync.WaitGroup)
	RegisterContext(ctx context.Context)
	StartGracefulShutdown(ctx context.Context)
}

func New() *Manager {
	return &Manager{
		wg:             &sync.WaitGroup{},
		shutdownSignal: make(chan os.Signal, 1),
	}
}

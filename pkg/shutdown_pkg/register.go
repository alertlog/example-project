package shutdown_pkg

import (
	"context"
	"sync"
)

// AddHandler добавляем функцию чтобы потом корректно завершить работу
func (m *Manager) AddHandler(handler ShutdownHandler) {
	m.handlers = append(m.handlers, handler)
}

// RegisterWaitGroup регистрируем WaitGroup
func (m *Manager) RegisterWaitGroup(wg *sync.WaitGroup) {
	m.wg.Add(1)
	go func() {
		wg.Wait()
		m.wg.Done()
	}()
}

// RegisterContext регистрируем контекст
func (m *Manager) RegisterContext(ctx context.Context) {
	m.wg.Add(1)
	go func() {
		<-ctx.Done()
		m.wg.Done()
	}()
}

package fs_pkg

import (
	"os"
)

// Проверка на существование папки
func CheckFolder(argPath string) error {
	_, error := os.Stat(argPath)
	if error != nil {
		return error
	}
	return nil
}

// создание вложенных папок
func CreateFolder(argPathFolder string) error {
	err := os.MkdirAll(argPathFolder, 0o777)
	if err != nil {
		return err
	}
	return nil
}

package notification_pkg

import (
	"context"
	"encoding/json"
	"go.opentelemetry.io/otel"
	"notification/config/secrets"
	"notification/pkg/http_pkg"
)

func (b *Notification) SendSms(ctx context.Context, params MessageBody) error {
	traceCtx, span := otel.Tracer("").Start(ctx, "SendSms")
	defer span.End()

	var req http_pkg.RequestArgs
	req.Url = secrets.Get("NOTIFICATION_MS_URL")
	req.Method = "POST"

	req.Headers = append(req.Headers, http_pkg.Header{
		Field: "Authorization",
		Value: http_pkg.Basic(secrets.Get("MS_LOGIN"), secrets.Get("MS_PASSWORD")),
	})
	req.Headers = append(req.Headers, http_pkg.Header{
		Field: "Content-Type",
		Value: "application/json",
	})

	body, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req.Body = body

	var resp string

	respBody, err := http_pkg.PlainRequest(traceCtx, req)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(respBody, &resp); err != nil {
		return err
	}

	return nil
}

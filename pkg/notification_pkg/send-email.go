package notification_pkg

import (
	"context"
	"gopkg.in/gomail.v2"
)

func (b *Notification) SendEmail(ctx context.Context, params NotificationEmail) error {
	m := gomail.NewMessage()
	m.SetHeader("From", FROM)
	m.SetHeader("To", params.Contact)
	m.SetHeader("Subject", params.Subject)
	m.SetBody("text/html", params.Message)
	d := gomail.NewDialer(SMTP_HOST, SMTP_PORT, FROM, PASSWORD)

	// Send the email
	if err := d.DialAndSend(m); err != nil {
		return err
	}

	return nil
}

package notification_pkg

type NotificationWhatsApp struct {
	Contact string
	Message string
}

type WazzupBody struct {
	ChannelId string `json:"channelId"`
	ChatType  string `json:"chatType"`
	ChatId    string `json:"chatId"`
	Text      string `json:"text"`
}

type Header struct {
	Field string
	Value string
}

type Method struct {
	GET    string
	POST   string
	PUT    string
	DELETE string
}

var METHOD = Method{
	GET:    "GET",
	POST:   "POST",
	PUT:    "PUT",
	DELETE: "DELETE",
}

type RequestArgs struct {
	Url           string
	Method        string
	SkipTlsVerify bool
	Body          []byte
	Headers       []Header
}

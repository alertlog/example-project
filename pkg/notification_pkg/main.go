package notification_pkg

import "context"

type Options struct {
	Url string
}

var NotificationUrl string

func Register(arg Options) {
	NotificationUrl = arg.Url
}

type Notification struct{}

type NotificationUseCase interface {
	SendSms(ctx context.Context, params MessageBody) error
	SendEmail(ctx context.Context, params NotificationEmail) error
	SendWhatsApp(ctx context.Context, params NotificationWhatsApp) error
}

func New() NotificationUseCase {
	return &Notification{}
}

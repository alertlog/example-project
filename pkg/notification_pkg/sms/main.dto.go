package sms

type Notification struct {
	Contact int
	Message string
}

type SmsBody struct {
	ApiID   string `json:"api_id"`
	Phone   int    `json:"to"`
	Message string `json:"msg"`
	Test    int    `json:"test"`
	JSON    int    `json:"json"`
}

type SmsSendResponse struct {
	Status     string `json:"status"`
	StatusCode int    `json:"status_code"`
}

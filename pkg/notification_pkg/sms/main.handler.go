package sms

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"notification/config/secrets"

	"strconv"
)

type Header struct {
	Field string
	Value string
}

type Method struct {
	GET    string
	POST   string
	PUT    string
	DELETE string
}

var METHOD = Method{
	GET:    "GET",
	POST:   "POST",
	PUT:    "PUT",
	DELETE: "DELETE",
}

type RequestArgs struct {
	Url           string
	Method        string
	SkipTlsVerify bool
	Body          *[]byte
	Headers       []Header
}

type SendSmsNotificationRequest struct {
	Contact int    `json:"contact"`
	Message string `json:"message"`
	Test    bool   `json:"test"`
}

func SendNotification(params SendSmsNotificationRequest) (*SmsSendResponse, error) {
	API_ID := secrets.Get("SMS_ID")
	SMS_URL := secrets.Get("SMS_URL")

	var auth []Header
	var arg RequestArgs

	values := url.Values{}

	values.Add("api_id", API_ID)
	values.Add("to", strconv.Itoa(params.Contact))
	values.Add("msg", params.Message)
	values.Add("json", "1")
	if params.Test {
		values.Add("test", "1")
	}

	arg.Url = fmt.Sprintf("%s?%s", SMS_URL, values.Encode())
	arg.Method = METHOD.GET
	arg.Headers = auth

	responseBody, err := Request(arg)
	if err != nil {
		return nil, err
	}
	var resp SmsSendResponse
	if err := json.Unmarshal(responseBody, &resp); err != nil {
		return nil, err
	}

	return &resp, nil
}

func Request(arg RequestArgs) ([]byte, error) {
	var reqBody []byte
	arg.Headers = append(arg.Headers, Header{
		Field: "Content-Type",
		Value: "application/json",
	})

	if arg.Body != nil {
		reqBody = *arg.Body
	}

	request, err := http.NewRequest(arg.Method, arg.Url, bytes.NewBuffer(reqBody))
	if err != nil {
		return nil, err
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	if arg.Headers != nil {
		for _, header := range arg.Headers {
			request.Header.Add(header.Field, header.Value)
		}
	}

	client := &http.Client{Transport: tr}
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	fmt.Println(string(body))

	return body, nil
}

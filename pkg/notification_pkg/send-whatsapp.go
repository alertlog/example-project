package notification_pkg

import (
	"context"
	"encoding/json"
	"fmt"
	"go.opentelemetry.io/otel"
	"notification/config/secrets"
	"notification/pkg/http_pkg"
	"notification/pkg/http_pkg/fiber_pkg"
)

func (b *Notification) SendWhatsApp(ctx context.Context, params NotificationWhatsApp) error {
	traceCtx, span := otel.Tracer("").Start(ctx, "SendWhatsApp")
	defer span.End()

	var CHANNEL_ID, CHAT_TYPE, WAZZAP_KEY, WAZZAP_URL string
	CHANNEL_ID = secrets.Get("CHANNEL_ID")
	CHAT_TYPE = secrets.Get("CHAT_TYPE")
	WAZZAP_KEY = secrets.Get("WAZZAP_KEY")
	WAZZAP_URL = secrets.Get("WAZZAP_URL")

	var body WazzupBody

	body.ChannelId = CHANNEL_ID
	body.ChatType = CHAT_TYPE
	body.ChatId = params.Contact
	body.Text = params.Message

	byteBody, err := json.Marshal(&body)
	if err != nil {
		return err
	}

	var auth []http_pkg.Header

	auth = append(auth, http_pkg.Header{
		Field: "Authorization",
		Value: fmt.Sprintf("Bearer %s", WAZZAP_KEY),
	})

	var arg http_pkg.RequestArgs

	arg.Body = byteBody
	arg.Url = WAZZAP_URL
	arg.Method = METHOD.POST
	arg.Headers = auth

	respBody, err := http_pkg.PlainRequest(traceCtx, arg)
	if err != nil {
		return err
	}

	var resp fiber_pkg.HttpResponse

	if err := json.Unmarshal(respBody, &resp); err != nil {
		return err
	}

	if resp.Status == "error" {
		return fmt.Errorf("error: %s", resp.Err.Code+": "+resp.Err.Message)
	}

	return nil
}

package notification_pkg

type NotificationEmail struct {
	Contact string
	Subject string
	Message string
}

const (
	FROM      = ""
	PASSWORD  = ""
	SMTP_HOST = ""
	SMTP_PORT = 0
)

package notification_pkg

type MessageBody struct {
	Notification NotificationReq `json:"notification"`
}

type NotificationReq struct {
	Address string `json:"address"`
	Body    string `json:"body"`
}

package http_pkg

import (
	"bytes"
	"context"
	"crypto/tls"
	b64 "encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
)

type Header struct {
	Field string
	Value string
}

type RequestArgs struct {
	Url           string
	Method        string
	SkipTlsVerify bool
	Body          []byte
	Headers       []Header
}

func Basic(login, password string) string {
	return fmt.Sprintf("Basic %s", b64.StdEncoding.EncodeToString([]byte(login+":"+password)))
}

func Request(arg RequestArgs, result interface{}) error {
	var reqBody []byte

	if arg.Body != nil {
		reqBody = arg.Body
	}

	request, err := http.NewRequest(arg.Method, arg.Url, bytes.NewBuffer(reqBody))
	if err != nil {
		return err
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	if arg.Headers != nil {
		for _, header := range arg.Headers {
			request.Header.Add(header.Field, header.Value)
		}
	}

	client := &http.Client{Transport: tr}
	response, err := client.Do(request)
	if err != nil {
		return err
	}

	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return err
	}
	// log.Printf("body: %v\n", string(body))

	if result == nil {
		return nil
	}

	if response.StatusCode != 200 {
		var errResp ResponseJson
		if err := json.Unmarshal(body, &errResp); err != nil {
			return err
		}
		return errors.New(errResp.Error.Message)
	}
	if err := json.Unmarshal(body, &result); err != nil {
		return err
	}

	return nil
}

func PlainRequest(ctx context.Context, arg RequestArgs) ([]byte, error) {
	request, err := http.NewRequestWithContext(ctx, arg.Method, arg.Url, bytes.NewBuffer(arg.Body))
	if err != nil {
		return nil, err
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	if arg.Headers != nil {
		for _, header := range arg.Headers {
			request.Header.Add(header.Field, header.Value)
		}
	}

	client := &http.Client{Transport: tr}
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

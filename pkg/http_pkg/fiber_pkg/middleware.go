package fiber_pkg

import (
	"strconv"

	"notification/config/secrets"
	"notification/pkg/constants_pkg"
	"notification/pkg/http_pkg"

	"github.com/gofiber/fiber/v2"
	basic "github.com/gofiber/fiber/v2/middleware/basicauth"
	jwtware "github.com/gofiber/jwt/v2"
)

func Auth() func(*fiber.Ctx) error {
	return jwtware.New(jwtware.Config{
		SigningKey:   []byte(secrets.Get("SECRET_JWT")),
		TokenLookup:  "header:Authorization",
		ErrorHandler: jwtError,
		ContextKey:   "user",
	})
}

func jwtError(c *fiber.Ctx, err error) error {
	if err.Error() == "Missing or malformed JWT" {
		return JSON(HttpResponse{
			Ctx:  c,
			Data: nil,
			Err: &http_pkg.Error{
				Code:    strconv.Itoa(140),
				Message: constants_pkg.HttpErrorMessage[140] + " " + err.Error(),
			},
			Status:     "fail",
			HttpStatus: fiber.StatusUnauthorized,
		})
	} else {
		return JSON(HttpResponse{
			Ctx:  c,
			Data: nil,
			Err: &http_pkg.Error{
				Code:    strconv.Itoa(139),
				Message: constants_pkg.HttpErrorMessage[139] + " " + err.Error(),
			},
			Status:     "fail",
			HttpStatus: fiber.StatusUnauthorized,
		})
	}
}

func BasicAuth() func(c *fiber.Ctx) error {
	return basic.New(basic.Config{
		Next: nil,
		Users: map[string]string{
			secrets.Get("MS_LOGIN"): secrets.Get("MS_PASSWORD"),
		},
		Realm:           "Restricted",
		Authorizer:      nil,
		Unauthorized:    nil,
		ContextUsername: "username",
		ContextPassword: "password",
	})
}

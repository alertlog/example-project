package fiber_pkg

import (
	"strconv"

	"notification/pkg/constants_pkg"
	"notification/pkg/http_pkg"

	"github.com/gofiber/fiber/v2"
)

type HttpResponse struct {
	Ctx        *fiber.Ctx
	Data       interface{}
	Err        *http_pkg.Error
	Status     string
	HttpStatus int
}

/*
Обертка для JSON ответа:
  - ctx - контекст Fiber
  - data - структура ответа
  - errors - список ошибок
  - status - статус выполнения операции
  - httpStatus - код HTTP ответа
*/
func JSON(arg HttpResponse) error {
	res := http_pkg.ResponseJson{}
	res.Data = arg.Data
	res.Error = arg.Err
	res.Status = arg.Status
	res.RequestId = arg.Ctx.GetRespHeaders()["X-Request-Id"]
	return arg.Ctx.Status(arg.HttpStatus).JSON(res)
}

const SUCCESS = "Success"
const FAIL = "fail"

// Обработчик возврата ответа ошибки
func ErrorsHandler(ctx *fiber.Ctx, data interface{}, err error) error {
	intErr, _ := strconv.Atoi(err.Error())
	if constants_pkg.HttpErrorMessage[intErr] != "" {
		return JSON(HttpResponse{
			Ctx:  ctx,
			Data: data,
			Err: &http_pkg.Error{
				Code:    strconv.Itoa(intErr),
				Message: constants_pkg.HttpErrorMessage[intErr],
			},
			Status:     FAIL,
			HttpStatus: fiber.StatusBadRequest,
		})
	}
	return JSON(HttpResponse{
		Ctx:  ctx,
		Data: data,
		Err: &http_pkg.Error{
			Code:    strconv.Itoa(400),
			Message: err.Error(),
		},
		Status:     FAIL,
		HttpStatus: fiber.StatusConflict,
	})
}

// Обработчик возврата успешного овтета
func SuccessHandler(ctx *fiber.Ctx, data any) error {
	return JSON(HttpResponse{
		Ctx:        ctx,
		Data:       data,
		Err:        nil,
		Status:     "success",
		HttpStatus: fiber.StatusOK,
	})
}

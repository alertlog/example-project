package http_pkg

type ResponseJson struct {
	Status    string      `json:"status" example:"success"`
	Error     *Error      `json:"error"`
	RequestId string      `json:"requestId" example:"efef-tht7h-thh-7dfdg"`
	Data      interface{} `json:"data"`
}

type ResponseErrJson struct {
	Status    string      `json:"status" example:"fail"`
	Error     *Error      `json:"error"`
	RequestId string      `json:"requestId" example:"efef-tht7h-thh-7dfdg"`
	Data      interface{} `json:"data"`
}

type Response400Json struct {
	Status    string   `json:"status" example:"fail"`
	Errors    []string `json:"errors" example:"Unauthorized user"`
	RequestId string   `json:"requestId" example:"efgrh-7hth-th6h-ghfg5"`
	Data      Empty    `json:"data"`
}

type Response500Json struct {
	Data      Empty    `json:"data"`
	Status    string   `json:"status" example:"fail"`
	Errors    []string `json:"errors" example:"Internal Server Error"`
	RequestId string   `json:"requestId" example:"fdfs-jyjy-th6hh-ijf5"`
}

type Error struct {
	Code    string `json:"code" example:"409"`
	Message string `json:"message"`
}

type Empty struct{}

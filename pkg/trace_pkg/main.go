package trace_pkg

import (
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/zipkin"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.17.0"
)

type RegisterOptions struct {
	ApiUrl      string
	ServiceName string
}

// var logger = log.New(os.Stdout, "zipkin-trace-log", log.Ldate|log.Ltime|log.Llongfile)

// Регистрация трассировщика
// https://github.com/open-telemetry/opentelemetry-go/blob/main/example/zipkin/main.go
func Register(arg RegisterOptions) error {
	exporter, err := zipkin.New(
		arg.ApiUrl,
		// zipkin.WithLogger(logger),
	)
	if err != nil {
		return err
	}

	batcher := sdktrace.NewBatchSpanProcessor(exporter)

	tp := sdktrace.NewTracerProvider(
		sdktrace.WithSpanProcessor(batcher),
		sdktrace.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceName(arg.ServiceName),
		)),
	)
	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))
	return nil
}

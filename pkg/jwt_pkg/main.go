package jwt_pkg

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"notification/config/secrets"
	"strconv"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

const (
	ACCESS  = "Access"
	REFRESH = "Refresh"
)

func CreateJWT(userId uint, typeJwt string) (string, error) {
	var signingKey string

	idString := strconv.Itoa(int(userId))
	var duration time.Duration

	switch typeJwt {
	case REFRESH:
		duration, _ = time.ParseDuration(secrets.Get("REFRESH_EXP_TIME"))
		signingKey = secrets.Get("SECRET_JWT_REFRESH")
	case ACCESS:
		duration, _ = time.ParseDuration(secrets.Get("ACCESS_EXP_TIME"))
		signingKey = secrets.Get("SECRET_JWT")
	}

	if signingKey == "" {
		return "", fmt.Errorf("empty signing key")
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		IssuedAt:  time.Now().Unix(),
		ExpiresAt: time.Now().Add(duration).Unix(),
		Subject:   idString,
	})

	return token.SignedString([]byte(signingKey))
}

func ParseAuthHeader(ctx *fiber.Ctx, typeKey string) (int, error) {
	header := ctx.Get("Authorization")
	if header == "" {
		return 0, fmt.Errorf("empty auth header")
	}

	headerParts := strings.Split(header, " ")
	bearer := headerParts[0]
	token := headerParts[1]
	if len(headerParts) != 2 || bearer != "Bearer" {
		return 0, fmt.Errorf("invalid auth header")
	}
	if len(token) == 0 {
		return 0, fmt.Errorf("token is empty")
	}

	return ParseJWT(token, typeKey)
}

func ParseJWT(argToken string, typeKey string) (int, error) {
	var signingKey string
	switch typeKey {
	case ACCESS:
		signingKey = secrets.Get("SECRET_JWT")
	case REFRESH:
		signingKey = secrets.Get("SECRET_JWT_REFRESH")
	}
	if signingKey == "" {
		return 0, fmt.Errorf("empty signing key")
	}

	token, err := jwt.Parse(argToken, func(t *jwt.Token) (i interface{}, err error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", t.Header["alg"])
		}
		return []byte(signingKey), nil
	})
	if err != nil {
		return 0, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return 0, fmt.Errorf("error get user claims from token")
	}

	if expiresAt, ok := claims["exp"]; ok && int64(expiresAt.(float64)) < time.Now().UTC().Unix() {
		return 0, fmt.Errorf("jwt is expired")
	}
	// String to int
	id, _ := strconv.Atoi(claims["sub"].(string))

	return id, nil
}

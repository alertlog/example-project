package loader

import (
	"encoding/json"
	"log"
	"notification/api/grpc"
	"notification/config/secrets"
	"notification/internal/infrastructure/db/psql"
	"os"
)

func Http() grpc.HttpConfig {
	result := grpc.HttpConfig{}
	b, err := os.ReadFile("./config/files/http.json")
	if err != nil {
		log.Fatal(err)
	}
	if err := json.Unmarshal(b, &result); err != nil {
		log.Fatal(err)
	}
	return result
}

func Psql() psql.PsqlConfig {
	result := psql.PsqlConfig{}
	b, err := os.ReadFile("./config/files/psql.json")
	if err != nil {
		log.Fatal(err)
	}
	if err := json.Unmarshal(b, &result); err != nil {
		log.Fatal(err)
	}
	return result
}

func Secrets() secrets.Connect {
	result := secrets.Connect{}
	b, err := os.ReadFile("./config/files/secrets.json")
	if err != nil {
		log.Fatal(err)
	}
	if err := json.Unmarshal(b, &result); err != nil {
		log.Fatal(err)
	}
	return result
}

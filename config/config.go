package config

import (
	"log"
	"notification/api/ampq/v1/subscribers"
	"notification/config/loader"
	"notification/config/secrets"
	"notification/internal/infrastructure/db/psql"
	v1 "notification/internal/infrastructure/db/psql/v1"
	"notification/internal/infrastructure/repository/v1/notification_repository"
	"notification/internal/queue/rabbit"
	"notification/pkg/telegram_pkg"
	"notification/pkg/trace_pkg"
	"os"
)

func Init() {
	// httpConfig := loader.Http()
	psqlConfig := loader.Psql()
	secretsConfig := loader.Secrets()

	secrets.Init(secretsConfig)
	telegram_pkg.Register(telegram_pkg.Options{
		ChatId:  secrets.Get("TELEGRAM_CHAT_ID"),
		BotKey:  secrets.Get("TELEGRAM_BOT_KEY"),
		AppName: secrets.Get("APP_NAME") + "/" + os.Getenv("APP_ENV"),
	})
	// defer grpc.Init(httpConfig)
	psql.Init(psqlConfig)

	// Инициализация RabbitMq
	// - создание очередей
	// - назначение прослушивателей
	rabbit.Init()

	// queueMaintenanceToBitrix := rabbit.New("MaintenanceToBitrix")
	queueNotificationSend := rabbit.New("notification_send")
	queueNotificationSendError := rabbit.New("notification_send_error")

	queueCodeSend := rabbit.New("code_send")
	queueCodeSendError := rabbit.New("code_send_error")

	go subscribers.RegisterNotificationSend(queueNotificationSend, queueNotificationSendError)
	go subscribers.RegisterCodeSend(queueCodeSend, queueCodeSendError)

	// Получение экземпляра хранилища сущностей Postgres
	psqlStoreV1 := v1.NewStore()

	// Регистрация репозитория User и передача хранилища сущностей
	notification_repository.Register(psqlStoreV1, queueNotificationSend)

	if err := trace_pkg.Register(trace_pkg.RegisterOptions{ApiUrl: secrets.Get("TRACE_URL"), ServiceName: secrets.Get("APP_NAME") + "/" + secrets.Get("APP_ENV")}); err != nil {
		log.Fatal(err)
	}
}

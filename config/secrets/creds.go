package secrets

var credentials = map[string]string{
	"APP_NAME":              "",
	"TRACE_URL":             "",
	"TELEGRAM_BOT_KEY":      "",
	"TELEGRAM_CHAT_ID":      "",
	"TELEGRAM_SEND_MESSAGE": "",
	"TELEGRAM_URL":          "",
	"POSTGRES_DB":           "",
	"POSTGRES_PORT":         "",
	"POSTGRES_HOST":         "",
	"POSTGRES_PASSWORD":     "",
	"POSTGRES_USER":         "",
	"REFRESH_EXP_TIME":      "",
	"ACCESS_EXP_TIME":       "",
	"SECRET_JWT_REFRESH":    "",
	"SECRET_JWT":            "",
	"SALT":                  "",
	"NOTIFICATION_LOGIN":    "",
	"NOTIFICATION_MS_URL":   "",
	"NOTIFICATION_PASSWORD": "",
	"RABBIT_HOST":           "",
	"RABBIT_USER":           "",
	"RABBIT_PASSWORD":       "",
	"RABBIT_PORT":           "",
	"SMS_ID":                "",
	"SMS_URL":               "",
	"CHANNEL_ID":            "",
	"CHAT_TYPE":             "",
	"WAZZAP_KEY":            "",
	"WAZZAP_URL":            "",
}

func Get(key string) string {
	return credentials[key]
}

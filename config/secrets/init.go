package secrets

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"notification/pkg/telegram_pkg"
	"os"
	"time"

	_ "github.com/lib/pq"
)

// func to connect to DB
type Connect struct {
	DSN      string `json:"dsn"`
	Name     string `json:"name"`
	Attempts int    `json:"attempts"`
	Timeout  int    `json:"timeout"`
}

func Init(arg Connect) {
	if os.Getenv("SECRET_DSN") == "" {
		log.Fatal("not found env SECRETS Credentials")
	}

	if arg.Name == "" {
		log.Fatal("empty field name service into secrets.json")
	}

	arg.DSN = os.Getenv("SECRET_DSN")

	if os.Getenv("APP_ENV") == "local" {
		localLoad()
	} else {
		db, _ := ConnectDb(arg)
		go update(db, arg)
	}
	log.Println("secrets connected")
}

func localLoad() {
	for k := range credentials {
		credentials[k] = os.Getenv(k)
	}
}

func ConnectDb(arg Connect) (*sql.DB, error) {
	dsn := arg.DSN

	var db *sql.DB

	for i := 0; i < arg.Attempts; i++ {
		connect, err := sql.Open("postgres", dsn)
		db = connect
		if i == arg.Attempts && err != nil {
			log.Fatal(err)
		} else if err == nil && connect != nil {
			break
		} else {
			<-time.After(time.Second * time.Duration(arg.Timeout))
		}
	}

	if err := db.Ping(); err != nil {
		log.Fatal("ping error: cannot connect to DB, err:", err)
	}

	if err := populateCreds(db, arg.Name); err != nil {
		log.Fatal(err)
	}

	return db, nil
}

func populateCreds(db *sql.DB, service string) error {
	data := make(map[string]string)
	value := ""
	key := ""

	query := fmt.Sprintf("SELECT key, value from secrets where service = '%s'", service)
	rows, err := db.Query(query)
	if err != nil {
		return fmt.Errorf("unable get data from DB: %v", err)
	}

	defer rows.Close()

	for rows.Next() {
		if err := rows.Scan(&key, &value); err != nil {
			return fmt.Errorf("scan error: %v", err)
		}
		data[key] = value
	}

	for key := range credentials {
		value, ok := data[key]
		if !ok {
			return fmt.Errorf("value type assertion failed: %s %s", key, value)
		}
		credentials[key] = value
	}

	return nil
}

func update(db *sql.DB, arg Connect) {
	tiker := time.NewTicker(time.Minute * time.Duration(arg.Timeout))
	count := 0
	for range tiker.C {
		err := populateCreds(db, arg.Name)
		if err != nil {
			log.Println(err)
			if count == 0 {
				_ = telegram_pkg.New().SendMessage(context.Background(), telegram_pkg.Message{Body: "Не удалось обновить секреты", Trace: err.Error()})
				count++
			}
		}
		if err == nil && count != 0 {
			_ = telegram_pkg.New().SendMessage(context.Background(), telegram_pkg.Message{Body: "Секреты обновлены", Trace: ""})
			count = 0
		}

	}
}

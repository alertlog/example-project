package rabbit

import (
	"context"
	"notification/pkg/logger_pkg"

	"go.opentelemetry.io/otel"

	amqp "github.com/rabbitmq/amqp091-go"
)

// Publisher отправляет сообщение в очередь
func (p *Rabbit) Publish(ctx context.Context, body []byte, queueName string) error {
	var logger = logger_pkg.New("")
	traceCtx, span := otel.Tracer("").Start(ctx, "Publish")
	defer span.End()

	if connect.IsClosed() {
		if err := rabbitConnect(); err != nil {
			logger.Log(err)
			return err
		}
	}

	// Создать канал amqp
	amqpChannel, err := connect.Channel()
	if err != nil {
		logger.Error(err)
		return err
	}

	defer amqpChannel.Close()

	// Определить очередь
	q, err := amqpChannel.QueueDeclare(
		queueName, // name
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		logger.Error(err)
		return err
	}

	// Метод публикации
	err = amqpChannel.PublishWithContext(traceCtx,
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Body:         body,
		})

	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

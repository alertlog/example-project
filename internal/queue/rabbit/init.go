package rabbit

import (
	"fmt"
	"log"
	"notification/config/secrets"
	"notification/pkg/logger_pkg"

	amqp "github.com/rabbitmq/amqp091-go"
)

var connect *amqp.Connection


func Init() {
	var logger = logger_pkg.New("")
	if err := rabbitConnect(); err != nil {
		log.Fatalf("failed to initialize RabbitMQ: %v", err)
	}
	logger.Log("Rabbit connected")
}

func rabbitConnect() error {
	var logger = logger_pkg.New("")
	url := fmt.Sprintf("amqp://%s:%s@%s:%s/", secrets.Get("RABBIT_USER"), secrets.Get("RABBIT_PASSWORD"), secrets.Get("RABBIT_HOST"), secrets.Get("RABBIT_PORT"))
	logger.Log(url)

	conn, err := amqp.Dial(url)
	if err != nil {
		logger.Error(err)
		return err
	}
	connect = conn

	return nil
}

type Rabbit struct {
	Queue string
}

func New(queueName string) *Rabbit {
	log.Printf("queue: %s started", queueName)
	return &Rabbit{
		Queue: queueName,
	}
}

func Close() {
	if connect != nil {
		connect.Close()
		log.Println("Rabbit close completed.")
	}
}

package rabbit

import (
	"context"
	"fmt"
	"log"
	"notification/pkg/logger_pkg"
	"notification/pkg/telegram_pkg"
	"os"
	"time"

	"go.opentelemetry.io/otel"
)

// Потребитель, обработчик принятых сообщений
func (p *Rabbit) Consume(callback func(ctx context.Context, message []byte) error, errorQueue *Rabbit) {
	var logger = logger_pkg.New("")
	if connect.IsClosed() {
		if err := rabbitConnect(); err != nil {
			logger.Error(err)
			return
		}
	}

	// Создать канал amqp
	amqpChannel, err := connect.Channel()
	if err != nil {
		logger.Error(err)
		return
	}

	defer amqpChannel.Close()

	// Определить очередь
	queue, err := amqpChannel.QueueDeclare(
		p.Queue, // name
		true,    // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	if err != nil {
		logger.Error(err)
		return
	}

	// Fair dispatch. Необходим для сбалансированного распределения сообщений между воркерами
	err = amqpChannel.Qos(1, 0, false)
	if err != nil {
		logger.Error(err)
		return
	}

	// Регистрация consumer
	messageChannel, err := amqpChannel.Consume(
		queue.Name, // queue
		"",         // consumer
		false,      // auto-ack
		false,      // exclusive
		false,      // no-local
		false,      // no-wait
		nil,        // args
	)
	if err != nil {
		logger.Error(err)
		return
	}

	stopChan := make(chan bool)
	/* Из канала перебираются сообщения и обрабатываются функцией callback
	Если при обработке происходит ошибка, то это сообщение отправляется в очередь Errors
	*/
	go func(callback func(ctx context.Context, message []byte) error, queueName string) {
		log.Printf("consumer %s, ready, PID: %d", queueName, os.Getpid())
		for message := range messageChannel {
			traceCtx, span := otel.Tracer("").Start(context.Background(), "ConsumeMessage")

			messageBody := fmt.Sprintf("Received a message: %s", message.Body)
			log.Printf("messageBody: %v\n", messageBody)

			// Задержка между обработкой сообщений 4 секунды
			time.Sleep(4 * time.Second)

			if err := callback(traceCtx, message.Body); err != nil {
				logger.Error(err)
				log.Printf("err: %v\n", err)
				if err := errorQueue.Publish(traceCtx, message.Body, queueName); err != nil {
					logger.Error(err)
					continue
				}

				if err := message.Ack(false); err != nil {
					log.Printf("Error acknowledging message : %s", err)
				} else {
					log.Printf("Acknowledged message")
				}

				_ = telegram_pkg.New().SendMessage(traceCtx, telegram_pkg.Message{Body: fmt.Sprintf("message processing error in queue: %s", message.RoutingKey), Trace: err.Error()})
				continue
			}

			if err := message.Ack(false); err != nil {
				log.Printf("Error acknowledging message : %s", err)
			} else {
				log.Printf("Acknowledged message")
			}

			span.End()
		}
	}(callback, errorQueue.Queue)

	// Stop for program termination
	<-stopChan
}

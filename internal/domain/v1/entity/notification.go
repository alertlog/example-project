package entity

type Notification struct {
	UserId         int    `gorm:"user_id"`
	Chanel         string `gorm:"chanel"`
	Message        string `gorm:"message"`
	Status         string `gorm:"status"`
	ErrDescription string `gorm:"err_description"`
}

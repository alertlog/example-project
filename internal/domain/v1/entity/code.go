package entity

type Code struct {
	UserId  int    `gorm:"user_id"`
	Chanel  string `gorm:"chanel"`
	CodeVal string `gorm:"code"`
}

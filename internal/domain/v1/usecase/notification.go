package usecase

import (
	"context"

	ntf "notification/pkg/service_pkg"
)

type NotificationUseCase interface {
	Check(ctx context.Context, arg *ntf.CheckCodeRequest) (ntf.CheckCodeResponse, error)
	HandleTaskNotificationSend(ctx context.Context, arg *ntf.QueueMessage) error
	HandleTaskCodeSend(ctx context.Context, arg *ntf.QueueMessage) error
	CreateTaskCodeSend(ctx context.Context, arg *ntf.QueueMessage) error
	CreateTaskNotificationSend(ctx context.Context, arg *ntf.QueueMessage) error
}

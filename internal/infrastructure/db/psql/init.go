package psql

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"time"

	"notification/config/secrets"
	"notification/internal/infrastructure/db/psql/v1/codes_entity"
	"notification/internal/infrastructure/db/psql/v1/notification_entity"

	"github.com/uptrace/opentelemetry-go-extra/otelgorm"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type PsqlConfig struct {
	Connect struct {
		User     string `json:"user"`
		Password string `json:"password"`
		Host     string `json:"host"`
		Port     int    `json:"port"`
		Name     string `json:"name"`
		Attempts int    `json:"attempts"`
		Timeout  int    `json:"timeout"`
	} `json:"connect"`
	Dump struct {
		Period int `json:"period"`
	} `json:"dump"`
}

var sqlDb *sql.DB

func CloseDbConn() {
	if err := Close(); err != nil {
		log.Println("Error closing database connection:", err)
	}
	log.Println("CloseDbConn completed.")
}

func Close() error {
	if sqlDb != nil {
		return sqlDb.Close()
	}
	return nil
}

func Init(arg PsqlConfig) {
	populateConfig(&arg)
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		arg.Connect.Host, arg.Connect.Port, arg.Connect.User, arg.Connect.Password, arg.Connect.Name)
	var connect *gorm.DB
	for i := 0; i < arg.Connect.Attempts; i++ {
		db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
		connect = db
		if i == arg.Connect.Attempts && err != nil {
			log.Fatal(err)
		} else if err == nil && db != nil {
			break
		} else {
			<-time.After(time.Second * time.Duration(arg.Connect.Timeout))
		}
	}
	log.Println("db connected")
	if err := connect.Use(otelgorm.NewPlugin()); err != nil {
		log.Fatal(err)
	}
	db, err := connect.DB()
	if err != nil {
		log.Fatal(err)
	}
	sqlDb = db
	autoMigrate(connect)
	registerEntity(connect)
}

// Передать необходимые данные для подключения к СУБД из секретов
func populateConfig(arg *PsqlConfig) {
	if arg == nil {
		return
	}
	arg.Connect.User = secrets.Get("POSTGRES_USER")
	arg.Connect.Password = secrets.Get("POSTGRES_PASSWORD")
	arg.Connect.Host = secrets.Get("POSTGRES_HOST")
	port, err := strconv.Atoi(secrets.Get("POSTGRES_PORT"))
	if err != nil {
		log.Fatal("psql - populateConfig: ", err)
	}
	arg.Connect.Port = port
	arg.Connect.Name = secrets.Get("POSTGRES_DB")
}

func autoMigrate(db *gorm.DB) {
	err := db.AutoMigrate(
		&notification_entity.NotificationEntity{},
		&codes_entity.CodeEntity{},
	)
	if err != nil {
		log.Fatal("models is not migrated", err)
	}
	log.Println("models is migrated")
}

func registerEntity(db *gorm.DB) {
	notification_entity.Register(db)
	codes_entity.Register(db)
}

func Ping() error {
	return sqlDb.Ping()
}

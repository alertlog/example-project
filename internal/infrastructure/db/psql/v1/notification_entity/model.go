package notification_entity

import (
	"notification/internal/domain/v1/entity"

	"gorm.io/gorm"
)

type NotificationEntity struct {
	entity.Notification
	gorm.Model
}

// Ручное указание названия таблиц
func (ntf *NotificationEntity) TableName() string {
	return "notifications"
}

// Переменная подключения к СУБД
var DB *gorm.DB

// Регистрация поделючения к СУБД
func Register(db *gorm.DB) {
	DB = db
}

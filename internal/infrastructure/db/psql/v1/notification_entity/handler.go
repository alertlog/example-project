package notification_entity

import "context"

func (ntf *NotificationEntity) Delete(ctx context.Context) error {
	return DB.WithContext(ctx).Delete(&ntf.Notification).Error
}

func (ntf *NotificationEntity) Create(ctx context.Context) error {
	if err := DB.WithContext(ctx).Create(&ntf).Error; err != nil {
		return err
	}
	return nil
}

package codes_entity

import "context"

func (code *CodeEntity) GetByIds(ctx context.Context) error {
	return DB.WithContext(ctx).Where("user_id = ? AND code_val = ?", code.UserId, code.CodeVal).First(&code).Error
}

func (code *CodeEntity) Delete(ctx context.Context) error {
	return DB.WithContext(ctx).Delete(&code).Error
}

func (code *CodeEntity) Create(ctx context.Context) error {
	if err := DB.WithContext(ctx).Create(&code).Error; err != nil {
		return err
	}
	return nil
}

package codes_entity

import (
	"notification/internal/domain/v1/entity"

	"gorm.io/gorm"
)

type CodeEntity struct {
	entity.Code
	gorm.Model
}

// Ручное указание названия таблиц
func (code *CodeEntity) TableName() string {
	return "codes"
}

// Переменная подключения к СУБД
var DB *gorm.DB

// Регистрация поделючения к СУБД
func Register(db *gorm.DB) {
	DB = db
}

package v1

import (
	"notification/internal/infrastructure/db/psql/v1/codes_entity"
	"notification/internal/infrastructure/db/psql/v1/notification_entity"
)

// Набор сущностей для работы с СУБД
type PsqlStore struct {
	NotificationEntityStore notification_entity.NotificationEntity
	CodeEntityStore         codes_entity.CodeEntity
}

func NewStore() *PsqlStore {
	return &PsqlStore{}
}

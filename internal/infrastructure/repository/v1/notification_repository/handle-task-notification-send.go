package notification_repository

import (
	"context"
	"fmt"
	"notification/pkg/notification_pkg"
	"notification/pkg/notification_pkg/sms"
	"notification/pkg/service_pkg"
	"strconv"
)

func (n *NotificationRepo) HandleTaskNotificationSend(
	ctx context.Context,
	arg *service_pkg.QueueMessage,
) error {
	storeNotification := n.psqlStore.NotificationEntityStore

	ntf := notification_pkg.New()

	switch arg.Chanel {
	case "sms":
		//msg := notification_pkg.MessageBody{
		//	Notification: notification_pkg.NotificationReq{
		//		Address: arg.Value,
		//		Body:    arg.Message,
		//	},
		//}
		contactValue := arg.Value
		contactInt, err := strconv.Atoi(contactValue)
		if err != nil {
			n.logger.Error(fmt.Errorf("fail notification_repository HandleTaskNotificationSend: strconv.Atoi: %w", err))
			return err
		}

		msg1 := sms.SendSmsNotificationRequest{
			Contact: contactInt,
			Message: arg.Message,
			Test:    true,
		}

		if _, err = sms.SendNotification(msg1); err != nil {
			n.logger.Error(fmt.Errorf("fail notification_repository HandleTaskNotificationSend: sms.SendNotification(msg1): %w", err))
			return err
		}
	case "email":
		msg := notification_pkg.NotificationEmail{
			Contact: arg.Value,
			Subject: "",
			Message: arg.Message,
		}

		if err := ntf.SendEmail(ctx, msg); err != nil {
			n.logger.Error(fmt.Errorf("fail notification_repository HandleTaskNotificationSend: ntf.SendEmail: %w", err))
			return err
		}
	case "wazzup":
		msg := notification_pkg.NotificationWhatsApp{
			Contact: arg.Value,
			Message: arg.Message,
		}

		if err := ntf.SendWhatsApp(ctx, msg); err != nil {
			n.logger.Error(fmt.Errorf("fail notification_repository HandleTaskNotificationSend: ntf.SendWhatsApp: %w", err))
			return err
		}
	}

	storeNotification.UserId = int(arg.UserId)
	storeNotification.Message = arg.Message
	storeNotification.Chanel = arg.Chanel
	storeNotification.Status = "success"

	err := storeNotification.Create(ctx)
	if err != nil {
		n.logger.Error(fmt.Errorf("fail notification_repository HandleTaskNotificationSend: store.Create: %w", err))
		return err
	}

	return nil
}

package notification_repository

import (
	"context"
	"encoding/json"
	"fmt"
	"notification/pkg/service_pkg"
)

func (n *NotificationRepo) CreateTaskNotificationSend(ctx context.Context, arg *service_pkg.QueueMessage) error {
	message, err := json.Marshal(&arg)
	if err != nil {
		n.logger.Error(fmt.Errorf("fail notification_repository CreateTaskNotificationSend: fail json.Marshal: %w", err))
		return err
	}

	if err := n.queue.Publish(ctx, message, "notification_send"); err != nil {
		n.logger.Error(fmt.Errorf("fail notification_repository CreateTaskNotificationSend: fail odir.queue.Publish: %w", err))
		return err
	}

	return nil
}

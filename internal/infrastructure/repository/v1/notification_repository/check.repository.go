package notification_repository

import (
	"context"
	"errors"
	"fmt"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
	ntf "notification/pkg/service_pkg"
)

func (n *NotificationRepo) Check(ctx context.Context, arg *ntf.CheckCodeRequest) (ntf.CheckCodeResponse, error) {
	storeCode := n.psqlStore.CodeEntityStore

	storeCode.UserId = int(arg.UserId)
	storeCode.Chanel = arg.Chanel
	storeCode.CodeVal = arg.Code

	err := storeCode.GetByIds(ctx)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			n.logger.Error(fmt.Errorf("fail notification_repository Check: fail store.GetByIds: %w", err))
			return ntf.CheckCodeResponse{}, status.Errorf(codes.NotFound, "fail")
		}

		n.logger.Error(fmt.Errorf("fail notification_repository Check: fail store.GetByIds: %w", err))
		return ntf.CheckCodeResponse{}, status.Errorf(codes.Internal, "fail")
	}

	err = storeCode.Delete(ctx)
	if err != nil {
		n.logger.Error(fmt.Errorf("fail ordering_users_documents_repository Check: fail store.Delete: %w", err))
		return ntf.CheckCodeResponse{}, status.Errorf(codes.Internal, "fail")
	}

	return ntf.CheckCodeResponse{Message: "success"}, nil
}

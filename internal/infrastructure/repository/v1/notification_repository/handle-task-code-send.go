package notification_repository

import (
	"context"
	"fmt"
	"notification/pkg/generate_pkg"
	"notification/pkg/notification_pkg/sms"
	"strconv"
	"strings"

	"notification/pkg/notification_pkg"
	"notification/pkg/service_pkg"
)

const CodeMessage = "Код подтверждения для ЛК Страховых компаний 1dogma.ru - %s."

func (n *NotificationRepo) HandleTaskCodeSend(
	ctx context.Context,
	arg *service_pkg.QueueMessage,
) error {
	storeNotification := n.psqlStore.NotificationEntityStore
	storeCode := n.psqlStore.CodeEntityStore

	ntf := notification_pkg.New()

	code, err := generate_pkg.GenerateIntsString(5)
	if err != nil {
		n.logger.Error(fmt.Errorf("fail notification_repository HandleTaskCodeSend: generate_pkg.GenerateIntsString: %w", err))
		return err
	}
	// если номер начинается на 7800, то делаем код 5555
	if strings.HasPrefix(arg.Value, "7800") {
		code = "55555"
	}

	switch arg.Chanel {
	case "sms":
		//msg := notification_pkg.MessageBody{
		//	Notification: notification_pkg.NotificationReq{
		//		Address: arg.Value,
		//		Body:    arg.Message,
		//	},
		//}
		contactValue := arg.Value
		contactInt, err := strconv.Atoi(contactValue)
		if err != nil {
			n.logger.Error(fmt.Errorf("fail notification_repository HandleTaskNotificationSend: strconv.Atoi: %w", err))
			return err
		}

		msg1 := sms.SendSmsNotificationRequest{
			Contact: contactInt,
			Message: fmt.Sprintf(CodeMessage, code),
			Test:    true,
		}

		if _, err = sms.SendNotification(msg1); err != nil {
			n.logger.Error(fmt.Errorf("fail notification_repository HandleTaskNotificationSend: sms.SendNotification(msg1): %w", err))
			return err
		}
	case "email":
		msg := notification_pkg.NotificationEmail{
			Contact: arg.Value,
			Subject: "",
			Message: fmt.Sprintf(CodeMessage, code),
		}

		if err := ntf.SendEmail(ctx, msg); err != nil {
			n.logger.Error(fmt.Errorf("fail notification_repository HandleTaskCodeSend: ntf.SendEmail: %w", err))
			return err
		}
	case "wazzup":
		msg := notification_pkg.NotificationWhatsApp{
			Contact: arg.Value,
			Message: fmt.Sprintf(CodeMessage, code),
		}

		if err := ntf.SendWhatsApp(ctx, msg); err != nil {
			n.logger.Error(fmt.Errorf("fail notification_repository HandleTaskCodeSend: ntf.SendWhatsApp: %w", err))
			return err
		}
	}

	storeNotification.UserId = int(arg.UserId)
	storeNotification.Message = "*"
	storeNotification.Chanel = arg.Chanel
	storeNotification.Status = "success"

	err = storeNotification.Create(ctx)
	if err != nil {
		n.logger.Error(fmt.Errorf("fail notification_repository HandleTaskCodeSend: storeNotification.Create: %w", err))
		return err
	}

	storeCode.UserId = int(arg.UserId)
	storeCode.Chanel = arg.Chanel
	storeCode.CodeVal = code

	err = storeCode.Create(ctx)
	if err != nil {
		n.logger.Error(fmt.Errorf("fail notification_repository HandleTaskCodeSend: storeCode.Create: %w", err))
		return err
	}

	return nil
}

package notification_repository

import (
	"notification/internal/domain/v1/usecase"
	v1 "notification/internal/infrastructure/db/psql/v1"
	"notification/internal/queue/rabbit"
	"notification/pkg/logger_pkg"
)

type NotificationRepo struct {
	psqlStore v1.PsqlStore
	logger    logger_pkg.Logger
	queue     rabbit.Rabbit
	traceId   string
}

var (
	ps v1.PsqlStore
	rb rabbit.Rabbit
)

// Регистрация репозитория - сохранения хранилища Postgres
func Register(argPs *v1.PsqlStore, argRab *rabbit.Rabbit) {
	ps = *argPs
	rb = *argRab
}

type Options struct {
	TraceId string
}

// Создание экземпляра репозитория User
func New(arg *Options) usecase.NotificationUseCase {
	var logger *logger_pkg.Logger
	if arg != nil {
		logger = logger_pkg.New(arg.TraceId)
	} else {
		logger = logger_pkg.New("")
	}
	return &NotificationRepo{
		psqlStore: ps,
		logger:    *logger,
		traceId:   arg.TraceId,
		queue:     rb,
	}
}

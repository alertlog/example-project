cd ..
# Lint
echo "starting lint"
golangci-lint run ./...
if [ $? -eq 0 ]; then
	echo "lint success"
else
	cd ./scripts
	$SHELL
fi

PROTO_DIR = api/grpc/v1/notification/proto/notification
OUTPUT_DIR = ./pkg/service_pkg

.PHONY: generate
generate: clean generate_proto

.PHONY: generate_proto
generate_proto:
	@echo "Generating protobuf and gRPC files..."
	protoc \
		--proto_path=$(PROTO_DIR) \
		--go_out=$(OUTPUT_DIR) \
		--go-grpc_out=$(OUTPUT_DIR) \
		--go_opt=paths=source_relative \
		--go-grpc_opt=paths=source_relative \
		$(PROTO_DIR)/notification.proto
	@echo "Generation complete."

.PHONY: clean
clean:
	@echo "Cleaning old generated files..."
	rm -rf $(OUTPUT_DIR)/*
	@echo "Clean complete."
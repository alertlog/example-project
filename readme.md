# Сервис уведомлений
__Внимание__, этот сервис не работоспособен на 100%, так для корректной работы
следующих интеграций необходимо подключение к определенным поставщикам:
- sms
- email
- whatsapp

## Описание
Сервис представляет из себя обработку уведомлений:
- асинхронное получение сообщений в очередь и их обработка
- отправка сообщений через различные каналы: __sms, email, whatsapp__
- сохранение результатов отправки
- синхронное получение информации о кодах подтверждения: __sms, email, whatsapp__

Основа архитектуры была взята от сюда [ссылка](https://github.com/golang-standards/project-layout/blob/master/README_ru.md), и адаптирована под текущие нужды.

## Запуск сервиса
1. Запустить локальные сервисы: __PostgreSQL__, __RabbitMQ__

```sudo docker-compose -f scripts/docker-compose.yml up```

2. Добавить переменные окружения

```
ACCESS_EXP_TIME=2h;
APP_ENV=local;
APP_NAME=notification;
CHANNEL_ID=;
CHAT_TYPE=;
NOTIFICATION_LOGIN=;
NOTIFICATION_MS_URL=;
NOTIFICATION_PASSWORD=;
POSTGRES_DB=test_db;
POSTGRES_HOST=localhost;
POSTGRES_PASSWORD=test;
POSTGRES_PORT=13000;
POSTGRES_USER=test;
RABBIT_HOST=localhost;
RABBIT_PASSWORD=test;
RABBIT_PORT=5601;
RABBIT_USER=test;
REFRESH_EXP_TIME=168h;
SALT=test;
SECRET_DSN=empty;
SECRET_JWT=access;
SECRET_JWT_REFRESH=refresh;
SMS_ID=;
SMS_URL=;
TELEGRAM_BOT_KEY=;
TELEGRAM_CHAT_ID=;
TELEGRAM_SEND_MESSAGE=/sendMessage;
TELEGRAM_URL=https://api.telegram.org/;
TRACE_URL=;
WAZZAP_KEY=;
WAZZAP_URL=
```
3. Сгенерировать файлы для __grpc__ ```make```
4. Запустить проект

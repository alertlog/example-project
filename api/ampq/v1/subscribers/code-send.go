package subscribers

import (
	"context"
	"encoding/json"
	"notification/internal/infrastructure/repository/v1/notification_repository"
	"notification/internal/queue/rabbit"
	"notification/pkg/service_pkg"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
)

func RegisterCodeSend(queue, errorQueue *rabbit.Rabbit) {
	queue.Consume(MessageHandlerCodeSend, errorQueue)
}

func MessageHandlerCodeSend(ctx context.Context, message []byte) error {
	traceCtx, span := otel.Tracer("").Start(ctx, "MessageHandlerCodeSend")
	defer span.End()

	task := service_pkg.QueueMessage{}
	if err := json.Unmarshal(message, &task); err != nil {
		return err
	}

	span.SetAttributes(attribute.String("request_id", task.TraceId))

	opt := &notification_repository.Options{TraceId: task.TraceId}
	n := notification_repository.New(opt)

	if err := n.HandleTaskCodeSend(traceCtx, &task); err != nil {
		return err
	}

	return nil
}

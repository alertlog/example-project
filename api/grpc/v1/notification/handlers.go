package notification

import (
	"context"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"google.golang.org/grpc/metadata"
	"notification/internal/infrastructure/repository/v1/notification_repository"
	ntf "notification/pkg/service_pkg"
)

type Server struct {
	ntf.UnimplementedNotificationServer
}

// Проверка кода
func (s Server) CheckCode(
	ctx context.Context,
	r *ntf.CheckCodeRequest,
) (*ntf.CheckCodeResponse, error) {
	traceCtx, span := otel.Tracer("").Start(ctx, "CheckCode")

	reqID := ""
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		if rID, ok := md["x-request-id"]; ok {
			reqID = rID[0]
		}
	}
	span.SetAttributes(attribute.String("request_id", reqID))
	defer span.End()

	opt := &notification_repository.Options{TraceId: reqID}
	n := notification_repository.New(opt)

	msg, err := n.Check(traceCtx, r)
	if err != nil {
		return nil, err
	}

	return &msg, nil
}

func (s Server) NotificationSend(
	ctx context.Context,
	r *ntf.QueueMessage,
) (*ntf.Response, error) {
	traceCtx, span := otel.Tracer("").Start(ctx, "NotificationSend")

	reqID := ""
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		if rID, ok := md["x-request-id"]; ok {
			reqID = rID[0]
		}
	}
	span.SetAttributes(attribute.String("request_id", reqID))
	defer span.End()

	opt := &notification_repository.Options{TraceId: reqID}
	n := notification_repository.New(opt)

	err := n.HandleTaskNotificationSend(traceCtx, r)
	if err != nil {
		return nil, err
	}

	return &ntf.Response{}, nil
}

func (s Server) CodeSend(
	ctx context.Context,
	r *ntf.QueueMessage,
) (*ntf.Response, error) {
	traceCtx, span := otel.Tracer("").Start(ctx, "CodeSend")

	reqID := ""
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		if rID, ok := md["x-request-id"]; ok {
			reqID = rID[0]
		}
	}
	span.SetAttributes(attribute.String("request_id", reqID))
	defer span.End()

	opt := &notification_repository.Options{TraceId: reqID}
	n := notification_repository.New(opt)

	err := n.CreateTaskCodeSend(traceCtx, r)
	if err != nil {
		return nil, err
	}

	return &ntf.Response{}, nil
}

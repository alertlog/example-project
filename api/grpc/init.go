package grpc

import (
	"context"
	"log"
	"net"
	ntf "notification/api/grpc/v1/notification"
	"strconv"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	pb "notification/pkg/service_pkg"
)

type HttpConfig struct {
	Port int `json:"port"`
	Log  struct {
		Folder string `json:"folder"`
		File   string `json:"file"`
	} `json:"log"`
}

var grpcServer *grpc.Server

// Shutdown Этот метод позволяет серверу выполнять любые текущие запросы, но прекращает принимать новые соединения.
func Shutdown() {
	log.Println("Graceful shutdown initiated...")

	if grpcServer != nil {
		grpcServer.GracefulStop()
	}

	log.Println("Shutdown completed.")
}

func GrpcLoggingInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	reqID := ctx.Value("x-request-id")
	log.Printf("[request_id %v; method %s; path %s]\n", reqID, info.FullMethod, ctx.Value("path"))

	md, _ := metadata.FromIncomingContext(ctx)
	log.Printf("Metadata: %v", md)

	return handler(ctx, req)
}

func Init(arg HttpConfig) {
	lis, err := net.Listen("tcp", ":"+strconv.Itoa(arg.Port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer = grpc.NewServer(
		grpc.UnaryInterceptor(GrpcLoggingInterceptor),
	)
	pb.RegisterNotificationServer(
		grpcServer,
		&ntf.Server{},
	)

	log.Printf("server listening at %v", lis.Addr())

	err = grpcServer.Serve(lis)
	if err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}

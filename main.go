package main

import (
	"context"
	"log"
	"notification/api/grpc"
	"notification/config"
	"notification/config/loader"
	"notification/internal/infrastructure/db/psql"
	"notification/internal/queue/rabbit"
	"notification/pkg/logger_pkg"
	"notification/pkg/shutdown_pkg"
	"sync"
)

func main() {
	// Прослушивание сигналов завершения работы
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	wg := &sync.WaitGroup{}

	// Инициализация менеджера приложения
	shutdownManager := shutdown_pkg.New()
	logger_pkg.Register()
	config.Init()

	go start()

	// регистрируем в менеджере функции поэтапного завершения работы сервиса
	shutdownManager.AddHandler(grpc.Shutdown)
	shutdownManager.AddHandler(rabbit.Close)
	shutdownManager.AddHandler(psql.CloseDbConn)

	// регистрируем группу ожиданий и контекст
	shutdownManager.RegisterWaitGroup(wg)
	shutdownManager.RegisterContext(ctx)

	// стартуем StartGracefulShutdown который ожидает сигнал
	shutdownManager.StartGracefulShutdown(ctx)

	log.Println("Service stopped")
}

func start() {
	grpc.Init(loader.Http())
}
